// javascript.js

canvas               = O('logo')
context              = canvas.getContext('2d')
context.font         = 'bold italic 75px Georgia'
context.textBaseline = 'top'
image                = new Image()
image.src            = 'globe.png'

image.onload = function()
{
  gradient = context.createLinearGradient(0, 0, 0, 100)
  gradient.addColorStop(0.00, '#4C33FF')
  gradient.addColorStop(0.55, '#33E9FF')
  context.fillStyle = gradient
  context.fillText("S   cial Netw   rk", 5, 5)
  context.strokeText("S   cial Netw   rk", 5, 5)
  context.drawImage(image, 50, 20)
  context.drawImage(image, 477, 20)
}

function O(i) { return typeof i == 'object' ? i : document.getElementById(i) }
function S(i) { return O(i).style                                            }
function C(i) { return document.getElementsByClassName(i)                    }
